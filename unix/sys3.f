!!-----------------------------------------------------------------------------
!!
	subroutine SYS_DIR(PATH, ROUTINE, ARG)  !!
!!      --------------------------------------
!!
	character*(*) PATH !! wildcard file specification
	external ROUTINE   !! routine to call with all matching files
	integer ARG        !! argument for ROUTINE

	character file*128, user*32, line*128, line0*128
	integer l, lun, cnt, pid, kill

	call sys_temp_name('clnup', file)
	call sys_delete_file(file)
	call sys_cmd('ls -alt '//path//' > '//file)
	call sys_get_lun(lun)
	line0=' '
	open(lun, file=file, status='old', readonly, err=9)
1	read(lun, '(a)', end=2) line
	call str_trim(line,line,l)
	call routine(line, arg)
	goto 1
2	close(lun)
9	call sys_free_lun(lun)
	call sys_delete_file(file)
	end
