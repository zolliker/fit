!!-----------------------------------------------------------------------------
!!
        subroutine SYS_DATE(YEAR, MONTH, DAY) !!
!!      -------------------------------------
!!
!! get actual date
!!
        integer YEAR, MONTH, DAY !! 4-Digits year, month and day

        integer tarray(9)
        external sys_time
        integer sys_time

	integer t

	t=sys_time()
        call ltime(t, tarray)
        day=tarray(4)
        month=tarray(5)+1   ! tarray(5): months since january (0-11)!
        year=tarray(6)+1900 ! tarray(6): years since 1900, no y2k problem
        end
