int MycTmpName(char *result, const char *name, int reslen);
/* generate a temporary filename containing 'name'.
 * the filename is stored in 'result' with less than 'reslen' characters.
 */
 
int MycCleanTmp(void);
/* deletes temporary files from closed sessions. files not beginning with a
 * dot will be kept at least for 7 days (if the system does not delete then)
 */ 


#ifdef __CYGWIN__
#define TEMP_PATH getenv("TEMP")
#else
#define TEMP_PATH "/tmp"
#endif
