// FORTRAN function variable interface for Mac OS X
// this file is created by fvi and should not be modified
void sys_call_c__(void (**rtn)(),char *a1,int a2)
       { if(*rtn){(*rtn)(a1,a2);};}
void sys_call_cc__(void (**rtn)(),char *a1,char *a2,int a3,int a4)
       { if(*rtn){(*rtn)(a1,a2,a3,a4);};}
void sys_call_i__(void (**rtn)(),int *a1)
       { if(*rtn){(*rtn)(a1);};}
void sys_call_ci__(void (**rtn)(),char *a1,int *a2,int a3)
       { if(*rtn){(*rtn)(a1,a2,a3);};}
void sys_call_iiieirrrr__(void (**rtn)(),int *a1,int *a2,int *a3,void (*a4)(),int *a5,float *a6,float *a7,float *a8
,float *a9)
       { if(*rtn){(*rtn)(a1,a2,a3,a4,a5,a6,a7,a8,a9);};}
float sys_rfun_rriii__(float (**rtn)(),float *a1,float *a2,int *a3,int *a4,int *a5)
       { return((*rtn)(a1,a2,a3,a4,a5));}
float sys_rfun_r__(float (**rtn)(),float *a1)
       { return((*rtn)(a1));}
