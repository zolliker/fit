!!-----------------------------------------------------------------------------
!!
	subroutine SYS_HOME(HOME)   !!
!!      =========================
!!
!! get home directory (+ dot) as prefix for preferences files

	character HOME*(*)      !! (out) filename

	integer l

	call sys_getenv('HOME',home)
	call str_trim(home, home, l)
	if (l .lt. len(home)-1) then
	  if (home(l:l) .ne. '/') then
	    home(l+1:l+1)='/'
	    l=l+1
	  endif
	  home(l+1:l+1)='.'
	  l=l+1
	endif
	end
