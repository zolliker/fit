!!-----------------------------------------------------------------------------
!!
	subroutine SYS_RENAME_FILE(OLD, NEW)      !!
!!      ====================================
!!
	character OLD*(*), NEW*(*)      !! (in) old, new filename

	call rename(OLD, NEW)
	end

!!-----------------------------------------------------------------------------
!!
	subroutine SYS_DELETE_FILE(NAME)      !!
!!      ================================
!!
	character NAME*(*)      !! (in) filename

	call unlink(NAME)
	end
