!!-----------------------------------------------------------------------------
!!
	subroutine SYS_GET_CMDPAR(STR, L)	!!
!!	---------------------------------
!!
	character*(*) STR !!
	integer L	!!

        integer i,iargc
        
	l=0
	str=' '
	do i=1,iargc()
	  if (l .lt. len(str)) then
	    call getarg(i, str(l+1:))
	    call str_trim(str, str, l)
	    l=l+1
	  endif
	enddo
	if (l .gt. 0) then
	  if (str(1:l) .eq. ' ') l=0
	endif
	end
