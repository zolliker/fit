	program terinq

	implicit none
	character name*64, gdev*32, pps*32, line*78
	integer rows, i, ll, l, cols, iostat

	integer getppid

	line=' '
	call sys_get_cmdpar(line, i)
	if (i .eq. 0) i=1

	call sys_loadenv
	call sys_temp_name('.terinq',name)
	call sys_open(1, name, 'wo', iostat)
	if (iostat .ne. 0) then
	  print *,'can not open ',name
	  stop 'CHO failed'
	endif

	if (line(1:i) .eq. 'gethost') then ! read host information from file
	  read (1,'(a)',end=3) line
	  rewind 1
	  i=index(line,'(')
	  if (i .ne. 0) then
	    line=line(i+1:)
	    i=index(line, ')')
	    if (i .gt. 1) then
	      call sys_setenv('REMOTEHOST', line(1:i-1))
	    endif
	  endif
3	  line=' '
	  i=1
	endif

	call cho_inq(line(1:i), gdev, pps, cols, rows)
	ll=0
        if (gdev .ne. ' ') then
          call sys_setenv('CHOOSER_GDEV',gdev)
	  call sys_setenv('PGPLOT_DEV','/'//gdev)
          call str_trim(gdev, gdev, l)
          line='Display type: '//gdev(1:l)
          ll=l+14
	endif
	call cho_vpp_cups(pps)
       	if (pps .ne. ' ') then
          if (ll .gt. 0) then
            line(ll+1:)=',  '
            ll=ll+3
          endif
          call str_trim(pps, pps, l)
          line(ll+1:)='Default Printer: '//pps(1:l)
          ll=ll+l+17
          call sys_setenv('CHOOSER_DEST',pps(1:l))
	  call sys_setenv('CHOOSER_PDEV','PS')
          write(1,'(a)') 'alias lp lp -d '//pps(1:l)
          write(1,'(a)') 'alias lpr lpr -P '//pps(1:l)
          write(1,'(a)') 'alias lpl lp -o landscape -d '//pps(1:l)
	endif
        if (cols .ne. 0 .or. rows .ne. 0) then
          if (ll .gt. 0) then
            line(ll+1:)=',  '
            ll=ll+3
          endif
	  if (rows .eq. 0) then
	    rows=24
	  else
	    write(1, '(a,i2)') 'stty rows ',rows
	  endif
	  if (cols .eq. 0) then
	    cols=80
	  else
	    write(1, '(a,i2)') 'stty columns ',cols
	  endif
          write(line(ll+1:),'(a,i4,a,i3)') 'Window size:',cols,'x',rows
          ll=ll+20
        endif
        if (ll .gt. 0) then
          print *
          print *,line(1:ll)
          print *
        endif
90	continue
	write(1,'(2a)') '/usr/bin/rm ',name
	close(1)
	call sys_saveenv
	call sys_clean_tmp
	end
