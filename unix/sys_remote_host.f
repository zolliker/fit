!!-----------------------------------------------------------------------------
!!
	subroutine SYS_REMOTE_HOST(STR, TYPE) !!
!!
!! get remote host name/number
!!
!! type: TN telnet, RT: decnet, LO: local, XW: X-window (ssh or telnet)
!!
	character STR*(*), TYPE*(*) !!

      character host*128, line*128, path*256, os*7
	integer i,j,lun,iostat

      integer sys_cmd
      external sys_cmd

	call sys_getenv('OS', os)

	call str_upcase(os, os)

	if (os .eq. 'WINDOWS') then

	  str='local'

	  type='LO'

	  return

	endif

 

      call sys_getenv('HOST', host)
	call sys_getenv('DISPLAY', str)
	i=index(str,':')
	type=' '
	if (i .ge. 1) then
	  if (i .eq. 1) then
	    str='localhost'
	  else
	    str=str(1:i-1)
	  endif
	  type='XW'
	  if (str .ne. 'localhost') goto 80 
	endif
	call sys_getenv('REMOTEHOST', str)
        if (str .eq. ' ') then
          call sys_temp_name('.whoami', path)
          call sys_delete_file(path)
	  i=sys_cmd('who -m > '//path)
          call sys_get_lun(lun)
          call sys_open(lun, path, 'r', iostat)
          if (iostat .ne. 0) goto 9
          read(lun,'(a)',end=9,err=9) line
9         close(lun)
          call sys_delete_file(path)
          i=index(line,'(')
          if (i .ne. 0 .and. i .lt. len(line)) then
            str=line(i+1:)
            i=index(str, ')')
            if (i .ne. 0) str(i:)=' '
          endif
        endif
	i=index(str,':')
	if (i .ne. 0) str(i:)=' '
	if (str .ne. ' ') then
	  if (type .eq. ' ') type='TN'
	else
	  str=host
	  if (type .eq. ' ') type='LO'
	endif

c   add domain to short host names
80	i=index(str, '.') 
	j=index(host, '.')
	if (j .gt. 0 .and. i .eq. 0) then
	  call str_trim(str, str, i)
	  str(i+1:)=host(j:)
	endif
	end
