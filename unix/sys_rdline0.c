#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "myc_str.h"
#include "myc_fortran.h"

void F_FUN(sys_rd_line)(F_CHAR(cmd), int *retlen, F_CHAR(prompt) F_CLEN(cmd) F_CLEN(prompt))
{
  char *line_read;
  char p0[64], p[64], buf[1024];

  STR_TO_C(p0, prompt);
  str_copy(p, "\n");
  str_append(p, p0);

  puts(p);
  line_read = fgets(buf, sizeof(buf), stdin);

  if (line_read)
  {  
    STR_TO_F(cmd, line_read);
    *retlen=strlen(line_read);
    if (*retlen>F_LEN(cmd)) *retlen=F_LEN(cmd);
  } else {
    *retlen=-1;
  }
}
