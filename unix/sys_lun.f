!!-----------------------------------------------------------------------------
!!
	subroutine SYS_GET_LUN(LUN)    !!
!!
!! allocate logical unit number
!!
	integer LUN     !! out

	logical*1 act(50:100)
	common/syslun/act
	data act/51*.false./

	integer l

	l=50
	do while (l .lt. 99 .and. act(l))
	  l=l+1
	enddo
	if (l .eq. 100) stop 'SYS_GET_LUN: no more luns available'
	lun=l
	act(l)=.true.
	end


!!-----------------------------------------------------------------------------
!!
	subroutine SYS_FREE_LUN(LUN)    !!
!!
!! deallocate logical unit number
!!
	integer LUN     !! in

	logical*1 act(50:100)
	common/syslun/act

	if (lun .lt. 50 .or. lun .gt. 99) then
	  stop 'SYS_FREE_LUN: illegal lun'
	endif
	if (act(lun)) then
	  act(lun)=.false.
	else
	  stop 'SYS_FREE_LUN: lun already free'
	endif
	end
