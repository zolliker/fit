!!-----------------------------------------------------------------------------
!!
	subroutine SYS_WAIT(SECONDS)	!!
!!      ============================
!! wait for SECONDS
	real SECONDS !! resolution should be better than 0.1 sec.

	real tim, del

	real secnds

	tim=secnds(0.0)
1	del=seconds-secnds(tim)
	if (del .ge. 0.999) then
	  call sleep(int(del))
	  goto 1
	endif
	if (del .gt. 0) then	
	  call usleep(int(del*1E6))
	  goto 1
	endif
	end
