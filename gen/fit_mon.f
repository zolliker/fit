	subroutine fit_mon(monit)

	implicit none

	include 'fit.inc'

	integer i
	real monit,ymon1,s

 1004	FORMAT (' Desired Monitor (default:',F12.0,' ): ',$)

	if (monit .eq. 0) then
	  WRITE (ISYSWR,1004) YMON
	  READ  (ISYSRD,'(F10.0)',END=999,err=999) YMON1 ! Scale YVAL to common monitor YMON1
	else
	  ymon1=monit
	endif
999	if (ymon1 .eq. 0 .or. ymon1 .eq. ymon) return
	if (ymon .eq. 0) then
	  ymon=ymon1
	  do i=1,npkt
	    rmon(i) = ymon1
	  enddo
	  return
	endif
	s=ymon1/ymon
	ymon=ymon1
	do i=1,npkt
	  sig(i) = sig(i)*s
	  YVAL(i)=YVAL(i)*s
	enddo
	end


	subroutine fit_auto_mon

	implicit none

	include 'fit.inc'

	real rbest, rdif, rmean, r, rd

	integer j

	rmean=0
	do j=nxmin,nxmax
	  rmean=rmean+rmon(j)
	enddo
	if (nxmax .gt. nxmin) rmean=rmean/(nxmax+1-nxmin)
	rbest=ymon
	rdif=abs(ymon-rmean)
	do j=nxmin,nxmax
	  r=rmon(j)
	  rd=abs(r-rmean)
	  if (rd .lt. rdif) then
	    rdif=rd
	    rbest=r
	  endif
	enddo

	if (rbest .ne. ymon) then
	  call fit_mon(rbest)
	  print *,'Overall Monitor changed to ',ymon
	endif
	end



	subroutine fit_usemon(imon)

	implicit none

	include 'fit.inc'

	integer imon,i

 10	format ('   0 Automatic'
	1      /'   1 Std. Monitor'
	1      /'   2 Primary Beam Monitor'
	1      /'   3 Auxilliary Beam Monitor'
	1      /'   4 Time'
	1      /'   5 Proton Current'
	1      /' Channel to use as Monitor [',i1,']: ',$)

	if (imon .eq. 0) then
	  call dat_used_mon(i)
	  print 10, i
	  read(isysrd,'(i10)',end=999,err=999) i
	  if (i .gt. 0) call dat_use_mon(i)
	else
	  call dat_use_mon(imon)
	endif
999	end
