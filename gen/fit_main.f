	subroutine fit_main
c	-------------------

	implicit none

	include 'fit.inc'

	character*8192 str
	integer l
	integer i
	logical plotit, exit_after
	
	call fit_init

	ififu=8
	str(1:1)=' '
	call sys_get_cmdpar(str(2:), l)
	i=index(str, ' -p ')
	if (i .ne. 0) then
	  str(i:i+3)=' '
	  plotit=.true.
	else
	  plotit=.false.
	endif
	i=index(str, ' -f ')
	if (i .eq. 0) then
	  i=index(str, ' -F ')
	  exit_after = .false.
	else
	  exit_after = .true.
	endif
	if (i .ne. 0) then
	  str(i:i+3)=' '
	  call str_first_nonblank(str, i)
	  if (i .eq. 0) i=1
	  call str_trim(str(i:),str(i:),l)
	  call fit_command(str(i:i+l-1))
	  if (.not. exit_after) then
	    call fit_command(' ')
	  endif
	else
	  call str_first_nonblank(str, i)
	  if (i .eq. 0) i=1
	  call fit_open(str(i:))
	  if (plotit) call fit_plot(' ')
	  call fit_command(' ')
	endif
	call fit_exit
	end
