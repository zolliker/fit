	subroutine make_vers(lunin)
	
	integer lunin, lunout
	character tab*1, vers*32, dat*8, tim*10
	integer l, iostat
	
	tab=char(9)
	lunout=2
	if (lunout .eq. lunin) then
	  print *,'lun mismatch'
	  stop 'error in MAKE_VERS'
	endif
	call sys_open(lunout, 'fit_vers.f', 'wo', iostat)
	if (iostat .ne. 0) then
	  print *,'cannot write fit_vers.f'
	  stop 'error in MAKE_VERS'
	endif

	read(lunin,'(a)') vers
	call str_trim(vers, vers, l)
	call date_and_time(dat, tim)
	vers(l+1:)=' ('//dat(1:4)//'-'//dat(5:6)//'-'//dat(7:8)//' '
	1//tim(1:2)//':'//tim(3:4)//':'//tim(5:6)//')'
	call str_trim(vers, vers,l)
	write(lunout,'(a,6(/,a))')
	1 '! DO NOT EDIT this subroutine.'
	1,'! It is automatically created with: make_custom fit.vers'
	1,tab//'subroutine fit_vers(version)'
	1,tab//'character version*(*)'
	1,tab//'version='''//vers(1:l)//''''
	1,tab//'end'
	close(lunin)
	close(lunout)
	end

	
	
