#include <stdlib.h>
#include <assert.h>

typedef void (*Routine)(void);

typedef struct Desc {
  Routine r;
  int t;
} Desc;

static Desc list0={0,0};
static Desc *list=&list0;
static int n=1;
static int m=0;

static int idx(Routine r, int t) {
  int i;
  Desc *p;
  
  for (i=0; i<n; i++) {
    if (list[i].r == r) {
      if (i>0) {
        list[i].t = t;
      }
      return i;
    }
  }
  
  if (n>=m) {
    if (m==0) {
      m=64;
    } else {
      m*=2;
    }
    p = calloc(m, sizeof(Desc));
    if (!p) return -1;
    for (i=1; i<n; i++) {
      p[i] = list[i];
    }
    if (list != &list0) free(list);
    list = p;
  }
  
  list[n].r = r;
  list[n].t = t;
  n++;
  return n-1;
}


