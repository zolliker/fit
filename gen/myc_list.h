void Lst_Ins(void *head, void *item, int pos);
int Lst_Find(void *head, void *key);
void Lst_Pos(void *head, int pos);

#define LstDecl(HEAD,TYP,CMP,KEY,KTYP) struct TYP##_HDR { \
   void (*ck)(); int (*cmp)(KTYP,KTYP);KTYP *kptr;int pos; \
   void *list;TYP *itm; TYP kitm;} HEAD \
   ={&Lst_Ins,CMP,&HEAD.kitm.KEY}
#define LstIns(HEAD,ITM,POS) Lst_Ins((LstHead*)&HEAD,(HEAD.itm=(ITM)),POS);
#define LstFind(HEAD,KEY) (HEAD.pos=Lst_Find((LstHead*)&HEAD,HEAD.kptr-&KEY,KEY))
#define LstPos(HEAD,POS) Lst_Pos((LstHead*)&HEAD,POS)
