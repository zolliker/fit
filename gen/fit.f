	program FIT				! change FIT to your own program name
!	-----------
!
! Simple user function example (straight line).
!
	implicit none
	real FIT_LIN_FUN
	external FIT_LIN_FUN			! change FIT_LIN_FUN to your own function name

	character vers*32
	integer i,l
!---
! Welcome message

	call fit_vers(vers)
	call str_trim(vers, vers, l)

	print '(X)'
	print '(X,2A)','Program FIT          Version ',vers(1:l)
	do i=1,l
	  vers(i:i)='-'
	enddo
	print '(X,2A/)','-----------------------------',vers(1:l)
!---
! Function title and parameter names
!
	call fit_userfun('STRAIGHT LINE', fit_lin_fun)	! function title, function
	call fit_userpar('Bg(0)')		! first parameter: background at zero
	call fit_userpar('dBg/dX')		! second parameter: slope
	call fit_main
	end



	real function fit_lin_fun(x,p,n,mode,cinfo)
!	-------------------------------------------

	implicit none

	real x		! x-value
	integer n	! number of parameters
	real p(n)	! parameters
	integer mode	! mode
	integer cinfo	! calculation information (see below)

	if (mode .eq. 0) then

! Define here your own function

	  fit_lin_fun=p(1)+x*p(2)

	elseif (mode .lt. 0) then

! Use this part to do some initialisations.
! (e.g. read files, write out comments on your user function)
! This section is called by FIT_FUN (command FUN)

	  print *
	  print *, 'to define your own user function',
	1          ' leave FIT and type MYFIT'
	  print *, 'Example: STRAIGHT LINE'

	endif
	end
