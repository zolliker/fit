      	subroutine fit_init
!       -------------------

	call fit_init0(.true.)
	end
	
      	subroutine fit_init_silent
!       --------------------------

	call fit_init0(.false.)
	end
	
      	subroutine fit_init0(show)
!       --------------------------

	logical show

	include 'fit.inc'

	integer i
	integer iostat
	character*8 value
	data nset/1/
	data isysrd/5/, isyswr/6/	! logical units
	data nstyl/7/
	data (styl(i),i=1,7)/5,6,7,4,3,2,1/
	data autostyle/1/
	data cinfo/0/
	data usernp/0/
	data usertit/'USER FUNCTION'/
	data filesave/' '/
	data nplug/0/
	data trfmode/0/, shift/0.0,0.0/
	data ncolor/0/
	data legend/0/
	data itit/' '/
	
	do i=1,5
	  isw(i)=0
	enddo
	iscx=0
	iscy=0
	up=1.0
	epsi=0.05
	vtest=0.01
	nxmin=1
	nxmax=0
	nu=0
	itaur=0
	nregion=0
	call sys_loadenv

	call sys_getenv('FIT_AUTOPLOT', value)
	autoplot = (value .ne. ' ' .and. value .ne. '0'
	1     .and. value(1:2) .ne. 'OF')

	call sys_getenv('FIT_COLORS', value)
	read(value, '(i8)', iostat=iostat) ncolor

	if (npd .eq. 0) then
	  call fit_userpdp('Numor')
	  call fit_userpdp('Temp')
	endif
c	call fit_sym
	if (show) call cho_choose(' ')
	end
