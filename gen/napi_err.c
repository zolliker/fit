#include <stdio.h>
#include <string.h>
#include "napi.h"
#include "myc_str.h"

static int doReport;
static int init=1;
static char report[2048];

void NXswitchedReport(void *pData, char *string)
{
  if (doReport) {
    printf("%s\n",string);
  } else if (NULL==strstr(report, string)) { /* append only new messages */
    str_append(report, string);
    str_append(report, "\n");
  }
}

void nxswitchreport(int *doreport)
{
  if (init) { init=0; NXMSetError(NULL, NXswitchedReport); };
  doReport = *doreport;
  if (!doReport) report[0]='\0';
}

void nxlistreport(void) {
  if (report[0]!='\0') {
    printf("--- Errors in NeXus:%s---\n", report);
  }
}

void nxswitchreport_(int *doreport)
{
  nxswitchreport(doreport);
}

void nxlistreport_(void) {
  nxlistreport();
}

/*
int nxbytesize(int *type)
{
  return DFKNTsize(*type);
}
*/
