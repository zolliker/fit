	subroutine dat_init
c
c initialize interfaces to all supported data file types
c 
	logical init/.true./
 
	if (init) then
	  call dat_tasmad
	  call dat_lnsp
	  call dat_d1a
	  call dat_nexus
	  call dat_oldtas
	  call dat_rita
	  call dat_sics
	  call dat_ccl
	  call dat_frm
	  call dat_5c2
	  call dat_inx
	  call dat_ida
	  call dat_fda
	  call dat_fullp
	  call dat_spec
	  call dat_2t
	  call dat_fit3
	  call dat_table
	  init=.false.
	endif
	end
