	subroutine fit_connect(conn)
!	----------------------------

	implicit none
	include 'fit.inc'
	character conc*1, conn*(*)
	integer i

 1002	FORMAT (4X,'Connect lines (Y/N, default:No): ',$)

	if (conn .eq. ' ') then
  102	  WRITE (ISYSWR,1002)
	  READ (ISYSRD,'(a1)',ERR=102,END=999) CONC
	else
	  conc=conn
	endif
        IF (conc .EQ. 'Y' .OR. conc .EQ.'y' .or.
	1    conc .eq. '1') then
	  do i=1,maxset
	    styl(i)=mod(styl(i),10)+sign(10,styl(i))
	  enddo
	else
	  do i=1,maxset
	    styl(i)=mod(styl(i),10)
	  enddo
	endif
	autostyle=0
999	end

