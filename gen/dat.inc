	integer maxtypes, maxcodes
	parameter (maxtypes=64, maxcodes=3)
	integer ntypes, dtype, year, last_type
	character spec*128
	integer desc_hdl(maxtypes)
	integer read_hdl(maxtypes)
	integer high_hdl(maxtypes)
	integer opts_hdl(maxtypes)

	common/dat_handlers/desc_hdl, read_hdl, high_hdl, opts_hdl
	1, ntypes, dtype, year, last_type, spec

	character specin*1, specout*1
	common /specinout/specin,specout
