	subroutine fit_bars(errb)
!	-------------------------

	implicit none
	include 'fit.inc'
	character errbar*1, errb*(*)
	integer i

 1002	FORMAT (4X,'Errorbars (Y/N, default:Yes): ',$)

	if (errb .eq. ' ') then
  102	  WRITE (ISYSWR,1002)
	  READ (ISYSRD,'(a1)',ERR=102,END=999) ERRBAR
	else
	  errbar=errb
	endif
        IF (errbar .EQ. 'N' .OR. errbar .EQ.'n' .or.
	1    errbar .eq. '0') then
	  do i=1,maxset
	    styl(i)=-abs(styl(i))
	  enddo
	else
	  do i=1,maxset
	    styl(i)=abs(styl(i))
	  enddo
	endif
	autostyle=0
999	end
