	subroutine FIT_FIX(k0)
C	----------------------

	integer k0

	include 'fit.inc'	

	integer i

	i = abs(K0)
	if (i .lt. 1 .or. i .gt. nu)  then
	  write(isyswr,'(4x,a,i3)') 'Illegal parameter no.:',i
	  return
	endif

	if (lcode(i) .le. 0)  then
	  if (icsw(i) .eq. 0) then
	    write(isyswr, '(4X,2A)') pnam(i),' is already fixed'
	  else
	    write(isyswr, '(4X,3A)')
	1     pnam(i),' is correlated to ',pnam(icto(i))
	  endif
	  return
	endif

10	lcode(i)=-lcode(i)
	icsw(i)=0
	if (werr(i) .ne. 0) then
	  werrs(i)=werr(i)
	  werr(i)=0
	endif
	if (isyswr .ne. 0) write (isyswr,502) pnam(i),u(i)
502	format (/4X,a,' fixed at ',f15.7)

	call extoin
	end
