#ifndef _MYC_STR_H_
#define _MYC_STR_H_

#define MYC_NAN (-1.125/1024./1024./1024.)

/*
  use these macros if DST is a fixed length character array
*/

#define str_trim(DST,SRC,L)             str_ntrim(DST,SRC,sizeof(DST),L)
#define str_pad(DST,SRC)                str_npad(DST,SRC,sizeof(DST))
#define str_split(DST,SRC,SEP)          str_nsplit(DST,SRC,SEP,sizeof(DST))
#define str_substitute(DST,SRC,OLD,NEW) str_nsubstitute(DST,SRC,OLD,NEW,sizeof(DST))
#define str_upcase(DST,SRC)             str_nupcase(DST,SRC,sizeof(DST))
#define str_lowcase(DST,SRC)            str_nlowcase(DST,SRC,sizeof(DST))
#define str_copy(DST,SRC)               str_ncpy(DST,SRC,sizeof(DST))
#define str_append(DST,SRC)             str_ncat(DST,SRC,sizeof(DST))


char *str_split1(char *str, char separator);
/*
  trims text before separator in *str and returns
  a pointer to the first character after separator
*/

char *str_splitx(char *str, char sep, char *list[], int *n);
/*
  split string into *n strings using separator sep.
  spaces at the end of the elements are trimmed
  attention: *str is modified ('\0' placed at the end of the elements)
  
  if *n separators are found, result points to string after *n-th separator
  else result is NULL
  *n contains number of elements stored in list
*/

int str_ntrim(char *dest, const char *src, int ldest, int lsrc);
/*
  copy characters 0 to lsrc-1 from src to dest (max ldest chars).
*/

int str_npad(char *dest, const char *src, int ldest);
/*
  copy src to dest and fill with spaces (fortran string format)
*/

char *str_nsplit(char *dst, const char *src, char sep, int dstlen);
/*
  returns a pointer to the text after the separator sep in *src
  and copies the text before the separator to *dst
  when *src does not contain the separator sep
  NULL is returned, and *dst is a copy of *src
*/

char *str_read_file(char *file);
/*
  return one string containing the contents of file *file
  comments separated by '!' are omitted. The caller must
  free the result after use.
*/

void str_replace_char(char *str, char ch, char rep);
/*
  replace all occurences of character ch by character rep in string *str
*/

int str_nsubstitute(char *result, char *str, char *old, char *new, int reslen);
/*
  replace every instance of old in str by new.
  the result must not overlap
  if the result would be longer than reslen, the result is en empty string
  and the return value is -1;
  else the return value is the length of the result.
  return one string containing the contents of file *file
  the contents are treated in the following way:
  - #0,#1,...#n is replaced by the corresponding argument *args[n] (n=0..nargs-1, nargs<10)
  - at the end of each line spaces and comments separated by ! are trimmed
*/

void str_nupcase(char *dst, const char *src, int dstlen);
/*
  convert *str to uppercase
*/

void str_nlowcase(char *dst, const char *src, int dstlen);
/*
  convert *str to lowercase
*/

#ifdef __VMS_VER
#if __VMS_VER<70000000

int strcasecmp(const char *str1, const char *str2);
/*
  compare *str1 with *str2
  the comparison is not case sensitive
  if result=0: strings are equal
  else
    result>0 <==> *str1>*str2
    first different character is at position abs(result)-1
*/

#else
#include <strings.h>
#endif /* __VMS_VER<70000000 */
#else
#include <strings.h>
#endif /* __VMS_VER */

int str_ncpy(char *dst, const char *src, int maxdest);
/*
  copy *src to *dest, maximal maxdest characters,
  it is guaranteed, that dst contains '\0'
*/

int str_ncat(char *dst, const char *src, int maxdest);
/*
  append *src to *dest, maximal maxdest characters,
  it is guaranteed, that dst contains '\0'
*/

#endif /* _MYC_STR_H_ */
