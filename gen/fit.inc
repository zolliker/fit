C	========================================================================
C			###### FIT.INC ######
C	========================================================================
C
	integer maxext,maxpar,maxpeak,maxplug,maxflen
	integer maxdat, maxset, maxpd
	real none
	parameter (maxdat=250000)       ! max. number of datapoints
	    ! this value is actually determined by fullprof synchrotron data
	    ! (number of points)*4 + number of reflections
	parameter (maxext=400)	        ! max. number of parameters
	parameter (maxpar=200)	        ! max. number of fitted parameters
	parameter (maxpeak=(maxext-2)/5)! max. number of peaks
	parameter (maxplug=16)	        ! max. number of plugin commands
	parameter (maxset=256)          ! max. number of datasets
	parameter (maxpd=8)             ! max. number of per-dataset parameters
	parameter (maxflen=8192)        ! max. filelist length
	parameter (none=-9.876543e-21)  ! undefined value

	real v(maxpar,maxpar)		! covariance matrix (must be first in common)
	integer npar, nu, ni		! number of internal, external parameters, informational parameters
	character*8 pnam(maxext)	! actual parameter names
	character*4 psho(maxext)	! actual short parameter names
	real u(maxext)			! parameter values
	real werr(maxext),werrs(maxext)	! parameter errors, saved errors
	real dirin(maxext)		! internal steps
	real alim(maxext), blim(maxext)	! limits
	integer lcode(maxext)		! <0: fixed, 1: normal, else: limited
	integer lcorsp(maxext)		! index of internal parameter
	integer icsw(maxext)		! correlation flag (0: not corr., 1: corr., -1: special I=M*G)
	integer icto(maxext)		! correlation pointer
	integer ncor, icord(maxext)	! correlation order
	real cfac(maxext)		! correlation factors
	real coff(maxext)		! correlation offsets
	real x(maxpar)			! internal parameters

	real amin			! function value
	real apsi, epsi, vtest		! convergence criteria
	integer ififu			! fit function (1: multi Gauss / Lorentz / Voigt
					! 5: crit. Exponent, 6: strange, 7: your own funct, 8: plot only
	integer nfree			! degrees of freedom
	integer nfcn, nfcnmx		! number of function calls, maximum
	integer isw(5)			! isw(1)=0,1(max number of fit calls exceeded)
					! isw(2)=0,1,2,3(normal convergence)
	real up				! value for error calc.
	integer itaur			! ?
	real sigma			! ?

	character*(maxflen) filnam	! file name
	character*(maxflen) fillis	! file name list
	character*(maxflen) filesave	! saved file name (for RANGE)
	real ymon, ymon0		! overall monitor (perm./temp.)
	integer titlen
	parameter (titlen=80)
	character*(titlen) itit		! title
	real temp,dtemp,wtemp,wavlen	! parameters for plot output
	integer load_state		! 0: no load, 1: accept, 2: done, 3: done, but load data from filnam

	real xval(maxdat)		! x-values
	real yval(maxdat)		! y-values
	real rmon(maxdat)		! original monitors
	real sig(maxdat)		! errors
	integer iset(maxdat)		! dataset number
	integer nset			! highest number of dataset

	integer npkt			! total number of points
	integer nback			! end of background points
	integer isysrd, isyswr		! logical units

	integer iscx, iscy		! auto scale flags
	integer nstyl, styl(maxset)	! dataset styles
	integer autostyle               ! automatic style (used for powder diffraction)
	logical autoplot                ! automatic plot after each command

	integer ncolor                  ! number of colors used (0 for b/w)
	integer npd			! number of per-dataset parameters
	real pdpar(maxpd, maxset)	! per-dataset parameters
	integer nmult			! number of tables
	integer cols(maxset)            ! number of cols
	integer rows(maxset)            ! number of rows
	integer legend                  ! legend parameter
	character legendlabels*2048	! custom legend
	real legendx,legendy		! legend position

	integer nxmin, nxmax		! fit window
	real xbeg, xend, ybeg, yend	! plot scale
	real yinteg, dyinteg		! integrated experimental intensity, error
	integer gradev			! graph device

	integer userfun			! user function number
	character*4 usersho(maxext)	! user function parameter short names
	character*32 userpar(maxext)	! user function parameter names
	integer usernp			! number of user function parameters
	integer usercct(maxext)		! calc. flags for user function
	character*16 usertit		! user function title
	integer cinfo			! calc. info
	real pold(maxext)		! last parameters

	integer actset			! set number of point calc.
	integer trfmode			! 0: normal, 1: logarithmic
	real shift(0:1)			! shifts for quasi 3-d

	logical argok			! command arguments ok
	logical quit			! quit flag
	character cmdline*8192		! commandline
	character separator*1           ! command argument separator
	integer cmdpos, cmdlen, linlen	! position, length of command, length of command line
	integer nplug                   ! number of plugin command routins
	integer plug_cmds(maxplug)      ! plug-in command routines


	integer maxregion
	parameter (maxregion=33)
	logical keepregion              ! keep window/excluded region on DAT command
	integer nregion			! region list length
	real regx1(maxregion)		! included region: regx1<=regx2 and regy1<=regy2
	real regx2(maxregion)		! excluded region: else
	real regy1(maxregion)		! regx1=regx2: infinite x-range
	real regy2(maxregion)		! regy1=regy2: infinite y-range

	common/fitint8/plug_cmds,userfun

	common/fitreal8/v

	common/fitchari/usertit,filesave,itit

	common/fitchar/cmdline,userpar,usersho,legendlabels
	1,psho,pnam,filnam,fillis,separator

	common/fitinti/nset,isysrd,isyswr,nstyl,styl,autostyle,cinfo
	1,usernp,nplug,trfmode,ncolor,legend

	common/fitint/npar,nu,ni,lcode,lcorsp,icsw,icto,ncor,icord
	1,ififu,nfree,nfcn,nfcnmx,isw,itaur
	1,iset,npkt,iscx,iscy,nxmin,nxmax,usercct
	1,actset, cmdpos, cmdlen, linlen, gradev, load_state
	1,npd,nmult,rows,cols,nback,nregion

	common/fitreal/u,werr,werrs,dirin,alim,blim,cfac,coff,x
	1,amin,apsi,epsi,vtest,up,sigma
	1,ymon,ymon0,temp,dtemp,wtemp,wavlen
	1,xval,yval,rmon,sig
	1,xbeg,xend,ybeg,yend,pdpar
	1,yinteg,dyinteg,pold,regx1,regx2,regy1,regy2
	1,legendx,legendy

	common/fitreali/shift
	
	common/fitlog/autoplot,quit,argok,keepregion
