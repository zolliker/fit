#ifndef _MEM_UTIL_H_
#define _MEM_UTIL_H_

#include <stdlib.h>
#include <string.h>

#ifdef FORTIFY
#include "fortify.h"
#endif

/* ------------------------------------------------------------
   these macros help for safer dynamic memory
   you may change these macros if you want to log dynamic memory access

*/

#define NEW(PTR,TYP) {TYP _0_={0}; ERR_SP(PTR=malloc(sizeof(*PTR))); *PTR=_0_; }
/*
  allocates and initializes an object of type TYP and make PTR point to it
  TYP must be defined with an appropriate typedef declaration, and
  INIT(TYP) must follow the declaration to initialize a dummy initializer
  object.
*/

#define NEW_STR(TO,FROM) {ERR_SP(TO=malloc(strlen(FROM)+1)); strcpy(TO,FROM); }

#define MALLOC(SIZE) malloc(SIZE)
#define FREE(PTR) free(PTR)

#endif /* _MEM_UTIL_H_ */
