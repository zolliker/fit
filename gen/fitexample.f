	program FIT				! change FIT to your own program name
!	-----------
!
! Simple user function example (voigtian peak).
!
	implicit none
	real FIT_USR_FUN
	external FIT_USR_FUN			! change FIT_USR_FUN to your own function name

	character vers*32
	integer i,l
!---
! Welcome message

	call fit_vers(vers)
	call str_trim(vers, vers, l)

	print '(X)'
	print '(X,2A)','Program FIT          Version ',vers(1:l)
	do i=1,l
	  vers(i:i)='-'
	enddo
	print '(X,2A/)','-----------------------------',vers(1:l)
!---
! Function title and parameter names
!
	call fit_userfun('EXAMPLE VOIGT', fit_usr_fun)	! function title, function
	call fit_userpar('B:Bg(pos)')		! 1 background at peak pos.
	call fit_userpar('D:dBg/dX')		! 2 background slope
	call fit_userpar('I:Int.Int.')		! 3 integrated intensity
	call fit_userpar('P:pos')		! 4 position
	call fit_userpar('G:gaussFW')		! 5 gaussian fwhm
	call fit_userpar('L:lorFW')		! 6 lorentzian fwhm
	call fit_main
	end



	real function fit_usr_fun(x,p,n,mode,cinfo)
!	-------------------------------------------

	implicit none

	real x		! x-value
	integer n	! number of parameters
	real p(n)	! parameters
	integer mode	! mode
	integer cinfo	! calculation information (see below)

	real voigt
	external voigt

	if (mode .eq. 0) then

! Define here your own function

	  fit_usr_fun=p(1)+p(2)*(x-p(4))+p(3)*voigt(x-p(4),p(5),p(6))

	elseif (mode .lt. 0) then

! Use this part to do some initialisations.
! (e.g. read files, write out comments on your user function)
! This section is called by FIT_FUN (command FUN)

	  print *
	  print *, 'Example: Single Voigtian'

	endif
	end
