	subroutine fit_bgedit(bgfile)

	include 'fit.inc'

	character bgfile*(*)

	integer l, i, iostat
	character fil*128

	if (bgfile .eq. ' ') then
	  write(6, '(x,a,$)') 'Enter background file: '
	  read(isysrd, '(a)',err=999,end=999) fil
	  if (fil .eq. ' ') return
	else
	  fil=bgfile
	endif
	nback=npkt
	call sys_open(1, fil, 'r', iostat)
	if (iostat .eq. 0) then
1	  if (nback .ge. maxdat) goto 9
	  read(1,*,err=9,end=9) xval(nback+1), YVAL(nback+1)
	  nback=nback+1
	  goto 1
9	  close(1)
	endif
	call fit_connect('Y')
	call fit_bars('N')
	call fit_plot('B')
	call str_trim(fil, fil, l)
	call sys_open(1, fil, 'w', iostat)
	if (iostat .ne. 0) goto 99
20	print *,'write background file ', fil(1:l)
	do i=npkt+1,nback
	  write(1, *) xval(i), YVAL(i)
	enddo
	close(1)
	goto 100
999	print *,'input error'
	goto 100
99	print *, 'cannot write to background file: ', fil(1:l)
100	npkt=nxmax
	end
