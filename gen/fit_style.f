	subroutine fit_style(nstyle, style)
!	-----------------------------------

	implicit none
	include 'fit.inc'
	integer nstyle, style(*)

	integer i,j, arg_styl

	if (nstyle .gt. 0) then
	  nstyl=nstyle
	  do i=1,nstyl
	    styl(i)=style(i)
	  enddo
	else
	  nstyl=0
	  do i=1,maxset
	    j=arg_styl(999)
	    if (j .ne. 999) then
	      styl(i)=j
	      nstyl=i
	    endif
	  enddo
	  if (nstyl .eq. 0) then
	    write(isyswr,'(8(/x,a))') 'Available Styles'
	1,'Numbers:    0    1    2    3     4    5    6       7      8'
	1,'Names:     none dot  plus star circle X  square triangle point'
	1,' '
	1,'Enter the name, number, or symbol for each dataset'
	1,'To disable error bars put a minus sign before,'
	1,'to connect points put a slash after the style name or number.'
	    do i=1,min(maxset,nset)
	      write(isyswr,'(a,i3,a)') '$Style for dataset',i,': '
	      read(isysrd,'(a)',end=999,err=999) cmdline
	      call str_trim(cmdline, cmdline, linlen)
	      if (cmdline(1:linlen) .ne. ' ') then
	        cmdpos=0
	        j=arg_styl(999)
	        if (j .ne. 999) then
	          styl(i)=j
	          nstyl=i
	        endif
	      endif
	    enddo
	  endif
	endif
999	if (nstyl .eq. 0) then
	  nstyl=1
	  styl(1)=5
	endif
	autostyle=0
	end


	integer function arg_styl(idefault)
!	-----------------------------------

	implicit none

	include 'fit.inc'

	integer idefault

	integer p,l,j,lincon
	logical bars
	character*8 snam(0:8)/'NO','DOT','PLUS','STAR','CIRCLE','X'
	1,'SQUARE','DELTA','POINT'/

	bars=.true.
	call arg_gen(p,l)
	if (cmdline(p+1:p+l) .eq. ' ') then
	  arg_styl=idefault
	  return
	endif
	lincon=0
	if (cmdline(p+l:p+l) .eq. '/') then
	  lincon=10
	  l=l-1
	endif
	if (cmdline(p+1:p+1) .eq. '-') then
	  p=p+1
	  l=l-1
	  bars=.false.
	endif
	if (cmdline(p+1:p+1) .eq. '1'
	1   .and. cmdline(p+1:p+l) .ne. '1') then
	  p=p+1
	  l=l-1
	  lincon=10
	endif
	l=min(l,8)
	do j=0,8
	  if (cmdline(p+1:p+l) .eq. snam(j)(1:l) .or.
	1     cmdline(p+1:p+l) .eq. char(48+j)) then
	    arg_styl=j+lincon
	    goto 10
	  endif
	enddo
	if (cmdline(p+1:p+l) .eq. ' ') then
	  arg_styl=idefault
	  if (arg_styl .ge. 20) return
	else
	  call arg_err(0)
	endif
10	if (.not. bars) arg_styl=-arg_styl
	end
