#ifndef _SYS_UTIL_H_
#define _SYS_UTIL_H_

/* 

   fortran interface stuff

     declare fortran character arguments as F_CHAR(arg)
     and at at the end for each character argument <arg> add
     int <arg>_len to the argument list

     Use macros STR_TO_C and STR_TO_F to convert from Fortran character strings
     to C character arrays and vice versa.

*/


#if defined __VMS

typedef struct { short size, dummy; char *text; } SysVmsChar;

#define F_CHAR(VAR) SysVmsChar *VAR
#define F_DCHAR(VAR,LEN) static char VAR##_str[LEN]; SysVmsChar VAR##_desc={LEN,270,&VAR##_str[0]}; SysVmsChar *VAR=&VAR##_desc
#define F_CLEN(VAR) 
#define F_ALEN(VAR) 
#define F_LEN(VAR) VAR->size
#define STR_TO_C(DST,SRC) str_ntrim(DST, SRC->text, sizeof(DST), SRC->size)
#define STR_TO_F(DST,SRC) str_npad(DST->text, SRC, DST->size)

#define F_DESCRIPTOR 1
#define F_UNDERSCORE 0

#elif defined __alpha || defined __unix  || defined __GNUC__

#define F_CHAR(VAR) char *VAR
#define F_DCHAR(VAR,LEN) char VAR[LEN]; int VAR##_len=LEN
#define F_CLEN(VAR) ,int VAR##_len
#define F_ALEN(VAR) ,VAR##_len
#define F_LEN(VAR) VAR##_len
#define STR_TO_C(DST,SRC) str_ntrim(DST, SRC, sizeof(DST), SRC##_len)
#define STR_TO_F(DST,SRC) str_npad(DST, SRC, DST##_len)

#define F_DESCRIPTOR 0

#if defined __alpha
#define F_UNDERSCORE 1
#else

#ifndef F_UNDERSCORE
#define F_UNDERSCORE 2

#endif

#endif

#else

#error "other machines are not supported"

#endif

#if F_UNDERSCORE == 0
#define F_FUN(A) A

#elif F_UNDERSCORE == 1
#define F_FUN(A) A##_

#elif F_UNDERSCORE == 2
#define F_FUN(A) A##__

#endif

#endif /* _SYS_UTIL_H_ */
