        SUBROUTINE fit_LIST
C       -------------------
	implicit none

	include 'fit.inc'

	integer i
	real yth,yth1,yth2

	real fifun ! function

	if (nu .gt. 0) then
 1000	  format (/,7x,'x',9x,'y',8x,'sig',7x,'yth',6x,'y-yth',
	1         3x,'(y-yth)/sig')
          write (isyswr,1000)
	  do i=nxmin,nxmax
	    actset=iset(i)
	    yth=fifun(xval(i))
	    yth1=YVAL(i)-yth
	    yth2=yth1/sig(i)
	    write (isyswr,1001) xval(i),YVAL(i),sig(i),yth,yth1,yth2
 1001	    format (1X,6F10.3)
	  enddo
	else
	  write(isyswr,1010) ymon
1010	  format (/'coulmn y is scaled to monitor ',f12.2
	1         /'mon is the measured monitor'/
	1         /,7x,'x',9x,'y',8x,'sig',7x,'mon',6x,'set')
	  do i=nxmin,nxmax
            write (isyswr,1011) xval(i),YVAL(i),sig(i),rmon(i),iset(i)
1011	    format (1x,3f10.3,f12.2,i6)
	  enddo
	endif

        end
