	SUBROUTINE FIT_EXIT
!	-------------------

	include 'fit.inc'
	character ans*1

	if (npkt .gt. 0) then
	  if (npkt .gt. 1000) then
	    write(isyswr, '(/X,A,$)')
	1    'Do you want to save current data and parameters [n]? '
	    read(isysrd, '(a)',err=9,end=9) ans
	    if (ans .ne. 'Y' .and. ans .ne. 'y') goto 9
	  endif
	  call fit_save('last.fit3')
	endif
9	call gra_print
	END
