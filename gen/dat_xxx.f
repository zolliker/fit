	subroutine dat_xxx
!	------------------

	external dat_xxx_desc
	external dat_xxx_opts
	external dat_xxx_high ! this line for raw data files only
	external dat_xxx_read

	integer dtype/0/

	call dat_init_desc(dtype, dat_xxx_desc)
	call dat_init_opts(dtype, dat_xxx_opts)
	call dat_init_high(dtype, dat_xxx_high) ! this line for raw data files only
	call dat_init_read(dtype, dat_xxx_read)
	end


	subroutine dat_xxx_desc(text)
!	-----------------------------
	character*(*) text  ! (out) description

!             type    description
!             ----------------------------------
	text='XXX     test example'
	end


	subroutine dat_xxx_opts
!	-----------------------
	print '(x,a)'
	1,'opts description'
	end


	subroutine dat_xxx_high(file, numor)
!	------------------------------------
! only to implement for raw data files
! returns highest NUMOR. FILE is the location of the file holding the last numor

	character*(*) file  ! (in) filename containing info for last numor
	integer numor       ! (out) last numor

	end
	

	subroutine dat_xxx_read
	1 (lun, forced, nread, putval, nmax, xx, yy, ss, ww)
!	----------------------------------------------------
	implicit none

	integer lun	! (in) logical unit number (file will be closed if successful)
	integer forced	! 0: read only if type is sure; 1: forced read
	integer nread	! (out) >=0: = number of points read, file closed
	                !       -1: not correct type, file rewinded
	                !       -2: correct type, but unreadable, file rewinded
	external putval ! (in) subroutine to put name/value pairs.
	                ! for numeric data:     call putval('name', value)   ! value must be real
			! for character data:   call putval('name=text', 0.0)
	integer nmax    ! max. number of points
	real xx(*)	! x-values
	real yy(*)	! y-values
	real ss(*)	! sigma
	real ww(*)	! weights  (original monitor)
