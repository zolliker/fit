	include 'napif.inc'

	integer fileid(NXhandlesize)
	integer lp, status, type
	character path*1024, name*64, class*64, axis1*64, xaxis*32
	character cdata*256, units*64, monitor*32, instr*1
	character sdate*20
	integer ivalue, axis_signal, rank, rank0, length, nframes
	real fvalue, ymon
	integer p_array(2), dim(32)

	common /dat_hdf_com/fileid,path,name,class,status,lp,type
	1,cdata,ivalue,axis_signal,rank,rank0,length,fvalue
	1,p_array,dim,nframes,ymon
	1,units,monitor,instr,axis1,xaxis,sdate
