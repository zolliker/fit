      program addit

*     replaces "powderplus"
*     (23.4.99) rekonstruiert am 7.7.99, geaendert am 15.7.99,28.9.99,7.3.00
*
*     > myfit addit.f  
*     > mv a.out addit 

      implicit none

      integer l
      character*36 spec
      character*256 file
      character*8192 line
      character*5 flag

      call fit_init

      call sys_get_cmdpar(line, l)
      call str_lowcase(line, line)
      if (line(1:l) .ne. ' ') then
        call fit_dat_options(line)
      endif
      call sys_getenv('dat_defspec',spec)
      if (spec.eq.' ') spec='DMC'
      call str_trim(spec,spec,l)            ! Laenge bestimmen (l)

  100 write(*,*)
      write(*,'(x,a)')'DMC = 1 / HRPT = 2'
      write(*,'(x,3a,$)')'Instrument (default: ',spec(1:l),'): '
      read(*,'(a)')flag                     !      ^ schreibt spec von
                                            !        Zeichen 1 bis l 

      call str_upcase(flag, flag)           ! schreibt Inhalt von flag gross 

      if (flag.eq.'1') flag='DMC'
      if (flag.eq.'2') flag='HRPT'
      if (flag.ne.' ') then
        if (flag.ne.'DMC' .and. flag.ne.'HRPT') goto 100
        spec=flag
      endif

      call sys_setenv('dat_defspec',spec)

C      call fit_dat(' ')
C      call fit_merge(0.02)
      if (spec .eq. 'DMC') then
        call fit_dat_merge(' ',0.025)          ! ersetzt fit_dat und fit_merge
      else
        call fit_dat_merge(' ',0.025)          ! ersetzt fit_dat und fit_merge
      endif
      call fit_auto_mon
      write(*,*)
      call fit_mon(0)

  101 write(*,'(x,a,$)')'Name of output file: '
      read(*,'(a)')file
      if (file.eq.' ') goto 101

      call fit_export(0,'lnsp',file)

      call str_trim(file, file, l)
      write(*,*)
      write(*,'(x,2a)')'new file:   ',file(1:l)
      write(*,*)

      end
