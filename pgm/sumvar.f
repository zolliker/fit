      program sumvar
!     --------------

      implicit none

      integer nmax
      parameter (nmax=10000)

      character filelist*2048, files*2048, spec*16, sumvars*256
      character var*64, filename*128
      integer ls, l, k, km, i, n, pin, pout, lun, iostat
      real xval(nmax), yval(nmax), sig(nmax), rmon(nmax)

      external list_values, list_vars, list_nix

      integer mcols
      parameter (mcols=32)
      integer ncol, nframes
      real cnts, fmt(mcols), vmin(mcols), vmax(mcols)
      character line*1024, line2*1024, names(mcols)*32, time*6
      common /sum_com/ncol, nframes, cnts, fmt, vmin, vmax
     &    ,line, line2, names, time

!     call fit_init
      call sys_get_cmdpar(files,l)
      call dat_def_options('entry=')

      if (files .eq. ' ') then
        call dat_ask_filelist(filelist, ' ')
        if (filelist .eq. ' ') goto 91
        call dat_silent
        pin=0
        pout=0
        call dat_open_next(filelist, pin, line, pout
     &      , list_nix, nmax, n, xval, yval, sig, rmon)
      endif
      call sys_getenv('dat_defspec', spec)
      call sys_getenv('sumvar', sumvars)
      if (sumvars .eq. ' ') call sys_getenv('sumvars', sumvars)
      if (sumvars .eq. ' ') then
        call sys_getenv('sumvar_'//spec, sumvars)
      endif
      if (sumvars .eq. ' ') then
        sumvars=
     &      'Numor:5,Date:16,Title:25,Temp:10.3,dTemp:8.3,sMon:10.'
      endif
      if (files .eq. ' ') then
        print *
        print *,'  Variables listed by default:'
        call str_trim(sumvars, sumvars, l)
        print '(x,a)',sumvars(1:l)
        print *
30      print *,'  Enter a new variable list, <ret> for default'
     &           ,', or ? for help:'
        read(*, '(a)', err=91, end=91) line
        if (line .eq. '?') then
          print *
     &,'--------------------------------------------------------------'
          print *
     &,'  You may configure the default with the environment variables'
     &,'  sumvar or sumvar_',spec
          print *
     &,'  Example (to be typed on the Unix prompt):'
          print *
          print '(x,3a)'
     &,'> setenv sumvars "',sumvars(1:l),'"'
          print *
          print *
     &,'  For each column, write the variable name and the column'
     &,'  width, separated by a colon. For numeric values, give'
     &,'  also the number of digits after decimal point, separated'
     &,'  with a point. The columns have to be separated by a comma.'
     &,'  The column title is right justified, if a point is present.'
          print *
          print *
     &,'  List of variables in the first file:'
          call dat_silent
          pin=0
          pout=0
          call dat_open_next(filelist, pin, files, pout
     &        , list_vars, nmax, n, xval, yval, sig, rmon)
          call list_vars('*', 0.0)
          print *
     &,'--------------------------------------------------------------'
          goto 30
        endif
        if (line .ne. ' ') sumvars=line
        print *
        print *,'Output file name (default: terminal):'
        read(*, '(a)', err=91, end=91) filename
        if (filename .eq. ' ') then
          lun=6
        else
          lun=1
          call sys_open(lun, filename, 'w', iostat)
          if (iostat .ne. 0) then
            print *,'can not open',filename
            stop
          endif
        endif
      else
        filelist=files
        lun=6
      endif     

      call str_trim(sumvars, sumvars, ls)
      sumvars(min(len(sumvars),ls+1):)=','
      
      ncol=0
      k=0
      l=0
      line=' '
35    km=index(sumvars(k+1:),',')
      if (km .gt. 0) then
      if (km .gt. 1 .and. ncol .lt. mcols) then
        ncol=ncol+1
        var=sumvars(k+1:k+km-1)
        i=index(var, ':')
        if (i .eq. 0) then
          call str_trim(names(ncol), var, n)
          fmt(ncol)=16.3
        else
          call str_trim(names(ncol), var(1:i-1), n)
          fmt(ncol)=0
          read(var(i+1:),*,err=36) fmt(ncol)
36          if (fmt(ncol) .eq. 0) fmt(ncol)=16.3
        endif
        i=int(fmt(ncol)+0.001)
        if (index(var, '.') .eq. 0) then ! left just
          line(l+1:l+i)=names(ncol)
        else
          line(l+max(0,i-n)+1:l+i)=names(ncol)
        endif
        call str_upcase(names(ncol), names(ncol))
        l=l+i+1
      endif
      k=k+km
      goto 35
      endif

38    if (l .le. 1) goto 91
      l=l-1
      print *
      write(lun, '(x,a)') line(1:l)
      do i=1,l
      line(i:i)='-'
      enddo
      write(lun, '(x,a)') line(1:l)

      pin=0
      pout=0
40    line=' '
      line2=' '
      call dat_def_options('entry=*')
      call dat_silent
      call dat_open_next(filelist, pin, files, pout
     &    , list_values, nmax, n, xval, yval, sig, rmon)
      call str_trim(line, line, l)
      if (line(1:l) .ne. ' ') then
      write(lun, '(a)') line(1:l)
      if (line2(1:l) .ne. ' ')
     &      write(lun, '(a)') line2(1:l)
      endif
      if (pin .le. len(filelist)) goto 40
91    end


      subroutine list_nix(name, value)
      character name*(*)
      real value
      end
      
      subroutine list_vars(name, value)

      character name*(*)
      real value

      integer l/0/,j
      character line*80
      save line, l

      if (name .eq. 'ShowLevel') return
      j=index(name, '=')-1
      if (j .le. 0) call str_trim(name, name, j)
      if (l+j .ge. 80 .or. name .eq. '*') then
      print *,line(1:l)
      l=0
      endif
      if (l .gt. 0) then
      line(l+1:l+1)=','
      l=l+1
      endif
      line(l+1:)=name(1:j)
      l=min(80,l+j)
      end


      subroutine list_values(name, value)

      character name*(*)
      real value

      integer k,i,l,j
      character unam*32, form*8, field*128
      real f
      
      integer mcols
      parameter (mcols=32)
      integer ncol, nframes
      real cnts, fmt(mcols), vmin(mcols), vmax(mcols)
      character line*1024, line2*1024, names(mcols)*32, time*6
      common /sum_com/ncol, nframes, cnts, fmt, vmin, vmax
     &    ,line, line2, names, time

      if (name .eq. 'ranges') then
        nframes=nint(value)
      elseif (name .eq. 'Counts') then
        cnts=value
      elseif (len(name) .gt. 5) then
        if (name(1:5) .eq. 'Date=') then
          time=name(17:)
        endif
      endif
      j=index(name, '=')
      if (j .gt. 1) then ! string
        call str_upcase(unam, name(1:j-1))
      else
        call str_upcase(unam, name)
      endif
      k=0
      do i=1,ncol
        f=fmt(i)+1
        l=int(f+0.001)
        if (l .ge. len(field)) l=len(field)
        if (unam .eq. names(i)) then
          if (j .gt. 0) then ! string
            field(1:l)=' '//name(j+1:)
          else
            if (f-l .lt. 0.04) then
              write(form, '(a,I2,a)') '(i',l,')'
              write(field(1:l), form) nint(value)
            else
              write(form, '(a,f5.1,a)') '(f',f,')'
              write(field(1:l), form) value
            endif
          endif
          if (field(1:1) .ne. '-') field(1:1)=' '
          if (line(k+1:k+l) .eq. ' ') then
            line(k+1:k+l)=field(1:l)
            vmin(i)=value
            vmax(i)=value
          elseif (j .eq. 0) then ! numeric
            if (line(k+1:k+l) .ne. field(1:l)) then
              if (value .gt. vmax(i)) then
                line2(k+1:k+l)=field(1:l)
              elseif (value .lt. vmin(i)) then
                if (line2(k+1:k+l) .eq. ' ')
     &              line2(k+1:k+l)=line(k+1:k+l)
                line(k+1:k+l)=field(1:l)
              endif
            endif
          elseif (line(k+1:k+l) .ne. field(1:l)) then ! string
            line2(k+1:k+l)=field(1:l)
          endif
!         goto 39
        endif
        k=k+l
        if (k .gt. len(line)) goto 39
      enddo
39    continue
      end
