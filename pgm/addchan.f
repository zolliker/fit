      program addchan

! compile with:
!     > myfit -o addchan addchan.f

      implicit none

      integer j,i,n,listflag
      character*256 list
      real twoth

      integer nmax
      parameter (nmax=16000)
      real xx(nmax)

      call fit_init

      call dat_ask_filelist(list,' ')
      listflag=0

      do j=1,999999

        call fit_dat_next_opt(list,listflag,1,0.0)
        if (listflag.eq.0) goto 101
        
        call fit_get_real('A4', twoth)
        call fit_get_array('X', xx, nmax, n)
        print *,'a4 ',twoth,n
        do i=1,n
          if (xx(i) .le. 500.) then
            xx(i)=xx(i)-twoth+1000.
          endif
        enddo
        call fit_put_array('X', xx, n)
        if (n+1600 .ge. nmax) then
          call fit_merge(0.05)
        endif
      enddo

  101 write(*,*)

      call fit_merge(0.05)
      call fit_get_array('X', xx, nmax, n)
      do i=1,n
        xx(i)=xx(i)-1000.
      enddo
      call fit_put_array('X', xx, n)

      call fit_export(0,'lnsp',' ')


      end
