      program subit

*     ~= old changei
*     range and steps of file2 are interpolated according to file1;
*     file2 is scaled with factor mon1/mon2
*     23.4.99 keller, modified 7.3.00
*
*     > myfit subit.f
*     > cp a.out subit

      implicit none

      real const
      character*78 file1,file2,file3,title
      character*16 cc

      write(*,*)
      write(*,'(x,a)')'____________________________________'
      write(*,*)
      write(*,'(x,a)')' File1 - (Mon1/Mon2)*File2 + const.'
      write(*,'(x,a)')'____________________________________'
      write(*,*)

  100 write(*,'(x,a,$)')'File1 / file to add: '
      read(*,'(a)')file1
      if (file1.eq.' ') goto 100

  101 write(*,'(x,a,$)')'File2 / file to subtract: '
      read(*,'(a)')file2
      if (file2.eq.' ') goto 101

      write(*,'(x,a,$)')'Value of additive constant (default: 0):  '
      read(*,'(a)')cc
      if (cc.eq.' ') cc='0.'
      read(cc,*)const

      write(*,'(x,a,$)')'Name of output file: '
      read(*,'(a)')file3
      if (file3.eq.' ') file3='diff.dat'

      write(*,'(x,a,$)')'Title of output file: '
      read(*,'(a)')title
      if (title.eq.' ') title='difference pattern'

      write(*,*)
      write(*,*)

      call fit_init
      call fit_dat(file1)
      call fit_subtract(file2)

C      write(*,*)
C      call fit_mon(0)

      call fit_add(const, 0)
      call fit_title(title)

      call fit_export(0,'dmc',file3)

      write(*,*)
      write(*,'(x,2a)')'new file:   ',file3
      write(*,*)

      end
