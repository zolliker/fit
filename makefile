# This file is included by the makefile.
# It forwards the execution to a version specific makefile.
# FIT_VERSION must be defined
# M. Zolliker 08.2003

VERS_UNDEFINED=
VERS_UNDEFINED$(FIT_VERSION)=version_undefined

MD=cd $(FIT_VERSION); \
  make -f $(SRC)makefile_$(FIT_VERSION) SRC=$(SRC)

# overwrite MD when FIT_VERSION undefined
MD$(FIT_VERSION)=@ true

PRE=$(VERS_UNDEFINED) obj/$(FIT_VERSION)

SRC=src/

.SUFFIXES: .none

default: $(PRE)
	$(MD)

%.o: $(PRE)
	$(MD) $@

%: $(PRE)
	$(MD) $@

.DEFAULT: $(PRE)
	$(MD) $@

tree: $(PRE)

obj/$(FIT_VERSION):
	@ ./maketree . $(FIT_VERSION)

makefile:
	@ echo makefile

version_undefined:
	@ echo ""
	@ echo "Usage:"
	@ echo ""
	@ echo "Creating objects and targets in separate subdirectories"
	@ echo ""
	@ echo "    setenv FIT_VERSION version_xxx     (assume we are using tcsh)"
	@ echo "    make [target]"
	@ echo ""
	@ echo "  where version_xxx is one of"
	@ echo ""
	@ ls -1 makefile_* | cut -b 10-99 | pr -t -o 4
	@ echo ""
	@ echo "  Remarks:"
	@ echo "    All objects for alpha version will be created in subdirectory obj/alpha/,"
	@ echo "    Tip: define FIT_VERSION at login"
	@ echo ""

